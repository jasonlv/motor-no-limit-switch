/********************************************************************
* author    : Yuanmin Lv
* attention : PIT0和PIT1两个定时器，主要用于定时，没有外部引脚与之对应
*             ECT是增强的TIM，可以内部时钟也可以外部脉冲
*             PIT0用于主线定时、PIT1用于小范围定时
*             AD结果需要实际看是增加还是减少
* function  : 上电后3秒内，连续按键move按下5次后，进入测试模式。5秒后
              进入正常模式、、、
              增加CAN通信，收到命令执行动作，若通过手动控制则发出相应命令
              增加隔屏、翻折屏、桌板，对应看协议
*********************************************************************/



#include <hidef.h>            /* common defines and macros */
#include <MC9S12XS128.h>      /* derivative information */
#include "derivative.h"       /* derivative-specific definitions */
#include "CAN.h" 

//==============================参数修改区===================================
#define ID                  0x010       //隔屏是0x010，桌板是0x012   0x11是翻折屏
#define Direction           0x01        //0表示左，1表示右
//===========================================================================


//收到的字节信息0x00 高4位是0表示左，为1表示右边，低4位位0表示下降，为1表示上升
char UpCommand = 1;
char DownCommand = 0;


struct can_msg msg_send, msg_get;

#define Move PORTA_PA1    //PA0-4在板子上初始化都是1
//此处做修改，由于板子上面move和stop反了，此处也调换move和stop两个引脚，
//原来move是 PA0,stop是PA1，，，，，
#define Stop PORTA_PA0

//原来的测试板子是up->PA2  down->PA3
//新板子是  up->PA4  down->PA5
#define Up PORTA_PA4
#define Down PORTA_PA5

#define AD_threshold 100         //电流AD采集结果的阈值，超过该阈值停机
#define pulseTotalMax 200   //定义捕捉脉冲的总数阈值，超过该阈值，停机

void ECT_Init(void);
void AD_init(void);
void IO_init(void);
void delay_ms(int ms);
void setbusclock(void);
void initPIT0(void);        //定时器0通道的定时器
void setTime0(int setTime);
void initPIT1(void);        //定时器1通道的定时器
void setTime1(int setTime);
void readATD(void);
void UpProcess(void);
void DownProcess(void);
void clearStopFlag(void);
void TESTPorcess(void);

Bool AD_OverFlag = FALSE;     //AD值超过阈值的标志
unsigned int AD_result;
unsigned int AD_average;
unsigned int AD_max;
unsigned int AD_temp; 
unsigned int pulseTotal0 = 0; 
unsigned int pulseTotal1 = 0;
unsigned int PulseInterval0 = 9999;
unsigned int PulseAverageTemp = 0;
unsigned int PulseAverage = 0;
unsigned int PulseInterval0Temp[20];
unsigned int Input_Num = 0; 

unsigned int timerGoal0 = 0;
unsigned int timerCount0 = 0;
unsigned int timerOverflow0 = 0;

unsigned int timerGoal1 = 0;
unsigned int timerCount1 = 0;
unsigned int timerOverflow1 = 0;

unsigned int times = 0;         //读取方波的次数

unsigned int UpTimes = 0;       //上升卡住次数
unsigned int DownTimes = 0;     //下降卡住次数

Bool MoveFlag = FALSE;
Bool StopFlag = FALSE;          //stop按键停止标志         
Bool DownFlag = FALSE;
Bool UpFlag = FALSE;

Bool StopTemp = FALSE;      //
Bool StopWhole = FALSE;     //彻底停止标志，需要断电重启
                                                                       


Bool stopFlag = FALSE;      //外力阻止的停止标志

Bool protect = FALSE;       //数据保护标志
Bool LimitUp = FALSE;       //触发上面限位开关标志
Bool LimitDown = FALSE;     //触发下面限位开关标志

Bool TESTFLAG = FALSE;        //测试功能标志位
unsigned int ButtonTimes = 0; //隐藏测试功能按键次数

int i=0;


void main(void)
{  
  setbusclock();
  delay_ms(10); 
  AD_init();
  ECT_Init();
  IO_init();  
  initPIT0();
  INIT_CAN0();
  EnableInterrupts; 
  PITCFLMT_PITE=1; //定时总中断开启
  delay_ms(10); 
  
  if(Direction) {      //right
    UpCommand = 0x11;
    DownCommand = 0x10;
  } else{             //left
    UpCommand = 0x01;
    DownCommand = 0x00;  
  }
  
  UpFlag = FALSE;
  DownFlag = FALSE;
  MoveFlag = FALSE;
  StopFlag = FALSE; 
  
//=================
    msg_send.id = ID; 
    if(ID == 0x011){
      msg_send.len = 1; //翻折屏是1个字节
    }
    else{
      msg_send.len = 2;  //隔屏和桌板都是2个字节
      msg_send.data[0] = Direction;// 0 for left 1 for right
    }
    msg_send.RTR = FALSE;
    msg_send.prty = 0;

//        delay_ms(1000);
//        if(!MSCAN0SendMsg(msg_send)) //发送过程出现错误
//            for(;;);
//        else
  
  
  
  ButtonTimes = 0;
  
  /*******定时3秒用于启动隐藏的测试功能**************/
  setTime0(30000);  
  while(!timerOverflow0){
    if(!Move){
      while(TRUE){
        delay_ms(100);
        if(Move){
          UpFlag = TRUE;
          break; //跳出按键判断
        }
      }
      ButtonTimes++;
    }
    if(ButtonTimes >= 5){
      TESTFLAG = TRUE;
    }
  }
  /*******进入隐藏的测试模式**************************/
  while(TESTFLAG)
  {
     TESTPorcess();
  }
  /*******进入正常模式********************************/
  while(!TESTFLAG) 
  {
    Input_Num = 0; 
    //等待按键
    for(;;)             
    {
      if(DownFlag) break;
      if(UpFlag) break;
      if(!Move){
        while(TRUE){
          delay_ms(200);
          if(Move){
            UpFlag = TRUE;
            if(ID == 0x011){
              msg_send.data[0] = UpCommand; //翻折屏是1个字节
            }
            else{
              msg_send.data[1] = UpCommand;  //隔屏和桌板都是2个字节
            }
            MSCAN0SendMsg(msg_send);//send CAN message
            break; 
          }
        }
        break;
      }
    }
    //上升
    while(UpFlag)       
    {  
      if(DownFlag) break;
      UpProcess();
    }
    //等待按键
    for(;;)             
    {
      if(DownFlag) break;
      if(UpFlag) break;
      if(!Move){
        while(TRUE){
          delay_ms(200);
          if(Move){
            DownFlag = TRUE;
            if(ID == 0x011){
              msg_send.data[0] = DownCommand; //翻折屏是1个字节
            }
            else{
              msg_send.data[1] = DownCommand;  //隔屏和桌板都是2个字节
            }
            MSCAN0SendMsg(msg_send);//send CAN message
            break;
          }
        }
        break;
      }
    }
    //下降
    while(DownFlag)     
    {
      if(UpFlag) break;             //up开启
        
      DownProcess();  
    }
    PORTB = 0x00;       //电机运行停止       0000 0000
    PITTF=0x01;         //清中断标志位PTF0	
	  PITCE_PCE0=0;       //定时器通道0使能，定时器开始定时。等于0时关闭计时//关闭定时器
    clearStopFlag();   
  } 
}

void TESTPorcess(void)
{
    PORTB = 0x03;     //启动电机向上运动并打开电视  0000 0011
    UpFlag = TRUE;
    clearStopFlag();//清夹住标志
    setTime0(60000); 
    for(;;)
    {  
      if(stopFlag)     //夹住了
      {     
          PORTB = 0x05;             //向下运动2秒钟
          for(i = 0; i<20; i++)
          {
            if(Up) {
              PORTB = 0x01;             //停止运动
              clearStopFlag();//清夹住标志
              break;     //touch Up button when downing
            }
            delay_ms(100);
          }
          PORTB = 0x01;             //停止运动
          clearStopFlag();//清夹住标志
          for(;;) 
          {
            if(!Move){
              while(TRUE){
                delay_ms(200);
                if(Move){ 
                  PORTB = 0x03;     //继续向上
                  setTime0(60000); 
                  break;    //跳出按键判断
                }
              }
              break;
            }
          }    
      }
      //补充电流AD值作为阈值限位
      readATD();
      if(AD_average > AD_threshold) AD_OverFlag = TRUE;
      if(Down) {            //碰到上面的限位开关
        AD_OverFlag = FALSE;    //清AD限位的标志
        UpFlag = FALSE;
        break;
      }
    }
    PORTB = 0x04;                 //点击向下运动 关闭电视       0000 0100
    DownFlag = TRUE;
    clearStopFlag();//清夹住标志
    setTime0(60000);
    for(;;)
    {
        if(stopFlag)   //夹住了
        {         
          PORTB = 0x00;             //停住
          clearStopFlag();//清夹住标志
          for(;;) 
          {
            if(!Move){
              while(TRUE){
                delay_ms(200);
                if(Move){ 
                  PORTB = 0x04;     //继续向下
                  setTime0(60000);  
                  clearStopFlag();//清夹住标志
                  break;    //跳出按键判断
                }
              }
              break;
            }
          }          
        }
        //补充电流AD值作为阈值限位
        readATD();
        if(AD_average > AD_threshold) AD_OverFlag = TRUE;
        if(Up) {
          AD_OverFlag = FALSE;    //清AD限位的标志
          DownFlag = FALSE;
          break;
        }
    } 
    PORTB = 0x00;//停止
    setTime0(50000); 
    while(!timerOverflow0);
    setTime0(50000); 
    while(!timerOverflow0);
}

void UpProcess(void) 
{
    clearStopFlag();//清夹住标志     
    PORTB = 0x03;     //启动电机向上运动并打开电视  0000 0011
    setTime0(60000);  
    for(;;)           //向上运动的循环
    {
      if(stopFlag)   //夹住了
      {
          clearStopFlag();          //清夹住标志    
          PORTB = 0x05;             //向下运动2秒钟
          for(i = 0; i<20; i++)
          {
            if(Up){
              PORTB = 0x01;             //停止运动
              clearStopFlag();//清夹住标志 
              break;     //touch Up button when downing
            }
            delay_ms(100);
          }
          PORTB = 0x01;             //停止运动
          clearStopFlag();//清夹住标志
          for(;;) 
          {
            if(!Move){
              while(TRUE){
                delay_ms(200);
                if(Move){
                  clearStopFlag();//清夹住标志
                  PORTB = 0x03;     //继续向上 
                  setTime0(60000);
                  break; //跳出按键判断
                }
              }
              break;
            }
          }
      }
      //补充电流AD值作为阈值限位
      readATD();
      if(AD_average > AD_threshold) AD_OverFlag = TRUE;
      if(Down || AD_OverFlag) //碰到上面的限位开关
      {
        AD_OverFlag = FALSE;    //清AD限位的标志
        PORTB = 0x01;           //停止
        clearStopFlag();//清夹住标志
        UpFlag = FALSE;
        break;                  //如果Down信号限位
      }
      if(DownFlag) //收到CAN的下降命令
      {
        PORTB = 0x01;           //停止
        clearStopFlag();//清夹住标志
        break;                  //如果Down信号限位
      }
      if(!Move) 
      {
        StopTemp = TRUE;
        PORTB = 0x01; //停止
        clearStopFlag();
        while(TRUE){
          delay_ms(200);
          if(Move){
            break; //跳出按键判断
          } 
        } 
      }
      while(StopTemp)     //紧急停止
      {
        if(DownFlag) 
        {
          PORTB = 0x01;           //停止
          clearStopFlag();//清夹住标志
          break;                  //如果Down信号限位
        }
        if(!Move)
        {
          while(TRUE){
            delay_ms(200);
            if(Move){
              PORTB = 0x03;     //继续向上 
              setTime0(60000);
              StopTemp = FALSE;
              clearStopFlag();//清夹住标志
              break; //跳出按键判断
            } 
          }        
          break; 
        }
      }
    }
}

void DownProcess(void)
{
      clearStopFlag();              //清夹住标志     
      PORTB = 0x04;                 //点击向下运动 关闭电视       0000 0100
      setTime0(60000); 
      for(;;)                       //向下运动的循环
      {
        if(stopFlag)   //夹住了
        {
          clearStopFlag();          //清夹住标志    
          PORTB = 0x00;             //停止运动
          for(;;) 
          {
            if(UpFlag)             //up限位开关按下
            {  
              clearStopFlag();//清夹住标志                
              PORTB = 0x00;             //限位开关
              break;                    //如果Up信号限位即拉低，则跳出
            }
          
            if(!Move){
              while(TRUE){
                delay_ms(200);
                if(Move){
                  PORTB = 0x04;     //继续向下
                  setTime0(60000);
                  clearStopFlag();//清夹住标志
                  break; //跳出按键判断
                }
              }
              break;
            }
          }       
        }
        //补充电流AD值作为阈值限位
        readATD();
        if(AD_average > AD_threshold) AD_OverFlag = TRUE;
        if(Up)             //下面的限位开关按下
        { 
          AD_OverFlag = FALSE;    //清AD限位的标志 
          clearStopFlag();//清夹住标志                
          PORTB = 0x00;             //限位开关
          DownFlag = FALSE;
          break;                    //如果Up信号限位即拉低，则跳出
        }
        if(UpFlag)             //收到CAN的向上的命令
        {  
          clearStopFlag();//清夹住标志                
          PORTB = 0x00;             //限位开关
          break;                    //如果Up信号限位即拉低，则跳出
        }

        if(!Move)       //按下move键
        {
          StopTemp = TRUE;
          PORTB = 0x00;
          clearStopFlag();
          while(TRUE){
            delay_ms(200);
            if(Move){
              break; //跳出按键判断
            } 
          } 
        }  
        while(StopTemp)     //紧急停止
        {
          if(UpFlag)             //up限位开关按下
          {  
            clearStopFlag();//清夹住标志                
            PORTB = 0x00;             //限位开关
            break;                    //如果Up信号限位即拉低，则跳出
          }
          if(!Move)
          {
            while(TRUE){
              delay_ms(200);
              if(Move){
                StopTemp = FALSE;
                PORTB = 0x04;     //继续向xia
                setTime0(60000);
                clearStopFlag();//清夹住标志 
                break; //跳出按键判断
              } 
            }
            break; 
          }
        }
      } 
}

void clearStopFlag(void) //清楚防夹的标志，用于下次防夹
{
  stopFlag = FALSE;
  times = 0;
  PulseAverageTemp = 0;
  protect = FALSE;
}

void readATD(void)
{
  unsigned int i;
  AD_result = 0;
  AD_temp = 0; 
  for(i = 0; i<10; i++)
  {
    while(!ATD0STAT0_SCF);    //等待转换结束            
    AD_result += ATD0DR0;     //获取电流值
    ATD0STAT0_SCF = 1;        //清除队列完成标志
    if(AD_temp < ATD0DR0)
    {
      AD_temp = ATD0DR0;
    }
  }
  AD_max = AD_temp;
  AD_average = AD_result / 10;
}

void initPIT0(void)     //定时中断初始化函数 20ms 定时中断设置
{
  PITCFLMT_PITE=0;      //定时总中断关闭				
  PITMTLD0=20-1;        //8位定时器初值设定（四个同道共用）。20分频， 16MHz/20=800KHz 
  PITLD0=80-1;          //第0同道16位定时器初值设定。   800000Hz/80 = 10000Hz	 即0.1ms
  PITINTE_PINTE0= 1;    //定时器中断通道0中断使能				
  PITCFLMT_PITE=1;      //定时器总中断打开
  //PITCE_PCE0=1;       //定时器通道0使能，定时器开始定时。等于0时关闭计时
} 

void initPIT1(void)//定时中断初始化函数 20ms 定时中断设置
{
  PITCFLMT_PITE=0;      //定时总中断关闭				
  PITMTLD0=20-1;        //8位定时器初值设定（四个同道共用）。20分频， 16MHz/20=800KHz
  PITLD1=80-1;          //第1同道16位定时器初值设定。   800000Hz/80= 10000Hz	 即0.1ms
  PITINTE_PINTE1= 1;    //定时器中断通道1中断使能				
  PITCFLMT_PITE=1;      //定时器总中断打开
  //PITCE_PCE1=1;       //定时器通道1使能，定时器开始定时。等于0时关闭计时
}

void setTime0(int setTime) 
{
	 
  timerOverflow0 = 0;         //定时溢出清零
  timerCount0 = 0;            //计数清零
  timerGoal0 = setTime;  //定时 timerGoal0 /10  毫秒   最大值不能超过65535
  PITCE_PCE0=1;               //定时器通道0使能，定时器开始定时。等于0时关闭计时 
   
}
void setTime1(int setTime)
{ 
  timerOverflow1 = 0;         //定时溢出清零
  timerCount1 = 0;            //计数清零
  timerGoal1 = setTime;       //定时 timerGoal1个20ms
  PITCE_PCE1=1;               //定时器通道0使能，定时器开始定时。等于0时关闭计时 
}

/*************************系统时钟初始化**********************/
void setbusclock(void)
{   
    CLKSEL=0X00;				// disengage PLL to system
    PLLCTL_PLLON=1;			// turn on PLL
    SYNR=0x00 | 0x01; 	// VCOFRQ[7:6];SYNDIV[5:0]
                        // fVCO= 2*fOSC*(SYNDIV + 1)/(REFDIV + 1)
                        // fPLL= fVCO/(2 × POSTDIV) 
                        // fBUS= fPLL/2 
                        // VCOCLK Frequency Ranges  VCOFRQ[7:6]
                        // 32MHz <= fVCO <= 48MHz    00
                        // 48MHz <  fVCO <= 80MHz    01
                        // Reserved                  10
                        // 80MHz <  fVCO <= 120MHz   11				
    REFDV=0x80 | 0x01;  // REFFRQ[7:6];REFDIV[5:0]
                        // fREF=fOSC/(REFDIV + 1)
                        // REFCLK Frequency Ranges  REFFRQ[7:6]
                        // 1MHz <= fREF <=  2MHz       00
                        // 2MHz <  fREF <=  6MHz       01
                        // 6MHz <  fREF <= 12MHz       10
                        // fREF >  12MHz               11                         
                        // pllclock=2*osc*(1+SYNR)/(1+REFDV)=32MHz;
    POSTDIV=0x00;       // 4:0, fPLL= fVCO/(2xPOSTDIV)
                        // If POSTDIV = $00 then fPLL is identical to fVCO (divide by one).
    _asm(nop);          // BUS CLOCK=16M
    _asm(nop);
    while(!(CRGFLG_LOCK==1));	  //when pll is steady ,then use it;
    CLKSEL_PLLSEL =1;		        //engage PLL to system; 
}

/************************IO口初始化**************************/
void IO_init(void)
{
  DDRB = 0xFF; //0x0000 0110       PB1,PB2
  PORTB = 0x00;//初始化都输出低电平
  DDRA = 0x00; //0x0000 0000       PA2,PA3口作为输入
  PORTA = 0x00;
}

/************************延时程序****************************/
void delay_ms(int ms)
{
   int ii,jj;
   if (ms<1) ms=1;
   for(ii=0;ii<ms;ii++)
     for(jj=0;jj<2670;jj++);    //busclk:16MHz--1ms
}

/***************************AD初始化**************************/
void AD_init(void)
{
  ATD0DIEN=0x00;//  禁止数字输入，在这里这句可要可不要
  ATD0CTL0=0x00;//  设置多通道的回转通道的截止位BIT[3:0]     
  ATD0CTL1=0x00;// 1，设置分辨率BIT[6;5];2，设置电容放电 BIT[4]    
  ATD0CTL2=0x40;// 1，设置快速清除标志位BIT[6]；2，转换序列完成中断允许BIT[1] 
  ATD0CTL3=0x88;// 1，结果对齐方式BIT[7]；2，转换序列长度BIT[6:3]
  ATD0CTL4=0x03;// 1，BIT[7]装换精度1-8,0-10 采样时间BIT[6:5]；2，时钟预分频BIT[4:0]
  ATD0CTL5=0x20;// 1，连续还是单次BIT[5]；2，单通道还是多通道BIT[4]；3，哪几个通道参与BIT[3:0]     
}

/***********************ECT初始化********************/
void ECT_Init(void){  
  
  TSCR2=0x04;                  //禁止溢出中断,分频系数16（16/16MHz)
  TIOS_IOS0=0;                 //通道0为输入捕捉
  TIOS_IOS1=0;                 //通道1为输入捕捉
  TCTL2=80;
  TCTL4=0x05;                  //通道0和1都是捕捉上升沿
  TIE_C0I=1;                   //通道0输入捕捉中断允许
  TIE_C1I=1;                   //通道1输入捕捉中断允许
  TSCR1=0x80;                  //使能定时器

  //TIOS=0X00;  //IOS7 IOS6 IOS5 IOS4 IOS3 IOS2 IOS1 IOS0
                // 0     0   0    0     0    0   0     0 
                // IOSn=0,n为输入捕捉（IC)通道  用于计数
                // IOSn=1,n为输出比较（OC)通道  用于定时
                // 将PT口作为输入捕捉
  //TSCR1=0X80; // TEN TSWAI TSFRZ TFFCA  PRNT  0  0  0 
                //   1   0     0     0      0   0  0  0
                // TEN=1   定时器使能  =0时禁止计时、计数
                // TSWAI=1  进入中断等待模式时 禁止计数  =0时允许
                //TSFRZ=0  冻结模式下允许计时器和计数器继续运行  =1时禁止 
                //PRNT =1  允许精确计时
  //TSCR2=0X04; //TOI 0 0 0  TCRE PR2 PR1 PR0
                // 0  0 0 0    0   1   0   0
                //TOI=0 禁止溢出中断 =1时 在TFLG2_TOF=1时发出中断请求
                //TCRE=0 禁止计数器复位，自由计数 =1时通道7成功输出后计数器被复位
                //PRn 预分频 Feci=Fbus/2^PR  这里是16分频
                
  //TCTL2=0X80; //OM3 OL3 OM2 OL2 OM1 OL1 OM0 OL0
                //1   0   0   0   0   0   0   0
                //每两位确定一个输出比较通道在成功发生比较后采取何种输出动作
                  
  //TCTL4=0X05; //EDG3B EDG3A EDG2B EDG2A EDG1B EDG1A EDG0B EDG0A
                //  0     0     0     0     0    1     0      1   
                //EDGnB=1,EDGnA=0 仅捕捉下降沿
                //EDGnB=0,EDGnA=1 仅捕捉上升沿
  //TIE=0X03;     //C7I C6I C5I C4I C3I C2I C1I C0I
                  // 0   0   0   0   0   0   0   0
                  //CnI=1允许标志位引起中断 =0时禁止
  //TFLG1=0X03;   //C7F～C0F
                  //CnF=1说明该通道有动作。写1清除该标志位
  //TC1=0x00f0;
}


/********************ECT通道0输入捕捉中断****************/
#pragma CODE_SEG __NEAR_SEG NON_BANKED    //中断函数置于非分页区内
interrupt VectorNumber_Vtimch0 void Timer0_Input0(void)
{
  TFLG1_C0F=1;                //清中断标志位
  if(!protect) 
  {
    int v = 0;
    if(PulseInterval0 > timerCount0) {
       v =   PulseInterval0 - timerCount0;
    }else{
      v = timerCount0- PulseInterval0;
    }
    if(v<=2){
      
      protect = TRUE;   
    } 
  }
  
  PulseInterval0 = timerCount0;
  if(protect)
  {
    if(times < 20)
    {
      PulseAverageTemp += PulseInterval0;
      times++;
      PulseAverage = PulseAverageTemp/times;    
    }
    if(times >= 20) 
    {
      if(UpFlag) 
      {  
        if(timerCount0 > (PulseAverage/14 + PulseAverage))    //阈值调大
        {
          stopFlag = TRUE;
        } 
      }
      if(DownFlag) 
      {
        if(timerCount0 > (PulseAverage/14 + PulseAverage))    //阈值调大
        {
          stopFlag = TRUE;
        } 
      }
    }
  }
  timerCount0 = 0;
  Input_Num++;
  pulseTotal0++;
  if(Input_Num>10000) {
    
    Input_Num = 0;
  }
}

/********************ECT通道1输入捕捉中断****************/
#pragma CODE_SEG __NEAR_SEG NON_BANKED    //中断函数置于非分页区内
interrupt VectorNumber_Vtimch1 void Timer0_Input1(void)
{
  TFLG1_C1F=1;        //清中断标志位
}
#pragma CODE_SEG DEFAULT

/********************ECT通道2输入捕捉中断****************/
#pragma CODE_SEG __NEAR_SEG NON_BANKED    //中断函数置于非分页区内
interrupt VectorNumber_Vtimch2 void Timer0_Input2(void)
{
  TFLG1_C2F=1;        //清中断标志位
}
#pragma CODE_SEG DEFAULT

/*******************计时器PIT0中断**********************/
#pragma CODE_SEG __NEAR_SEG NON_BANKED
void interrupt 66 PIT0Interrupt(void) //PIT0中断号是66
{ 	
  timerCount0++;
	PITTF=0x01;//清中断标志位PTF0
	if(timerCount0 >= timerGoal0)
	{	
	  PITCE_PCE0=0;//定时器通道0使能，定时器开始定时。等于0时关闭计时
	  timerOverflow0 = 1;    
	}	     
}
#pragma CODE_SEG DEFAULT

/*******************计时器PIT1中断**********************/
#pragma CODE_SEG __NEAR_SEG NON_BANKED
void interrupt 67 PIT1Interrupt(void) //PIT1中断号是67
{ 
  timerCount1++;  
	PITTF=0x02;//清中断标志位PTF1
	if(timerCount1 >= timerGoal1)
	{
	  PITCE_PCE1=0;//等于0时关闭定时器1通道的计时
	  timerOverflow1 = 1;    
	}	     
}
#pragma CODE_SEG DEFAULT

/*************************************************************/
/*                      中断接收函数                         */
/*************************************************************/
#pragma CODE_SEG __NEAR_SEG NON_BANKED
void interrupt CAN_receive(void) 
{
  if(MSCAN0GetMsg(&msg_get)) 
  {
    // 接收新信息
    if(msg_get.id == ID && (!msg_get.RTR)) 
    {
      if(ID == 0x011){//若是翻折屏，只接收一个字节
        if(msg_get.data[0] == 0x01)//若接受的是1,则表示上升，UpFlag置位，0表示下降DownFlag置位
          {
            UpFlag = TRUE;
            DownFlag = FALSE;
          }
          else if(msg_get.data[0] == 0x00){
            UpFlag = FALSE;
            DownFlag = TRUE;
          }
      }
      else{//隔屏和桌板 接收两个字节
        if(msg_get.data[0] == Direction) //判断左右
        {
          if(msg_get.data[1] == 0x01)//若接受的是1,则表示上升，UpFlag置位，0表示下降DownFlag置位
          {
            UpFlag = TRUE;
            DownFlag = FALSE;
          }
          else if(msg_get.data[1] == 0x00){
            UpFlag = FALSE;
            DownFlag = TRUE;
          }        
        }
      }            
    }
  }
}
#pragma CODE_SEG DEFAULT









